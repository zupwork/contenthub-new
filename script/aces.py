"""
ACES materials including:
- Catalog
"""
from functools import reduce


def size_to_kb(size):
    """ Return the size in Byte to KB
    """
    return int(size) / 1024


class Catalog(object):
    """ ACES Catalog
    """

    def __init__(self, catalog):
        """ Build a catalog
        catalog: dictionary as per returned
            from ACES Cloud /register
            It is a native JSON dictionary"""
        # Skip the first unuseful level
        # The catalog['catalog'] is a list of list of content
        self._catalog = catalog['catalog']
        # Flat catalog of content as a dict: {uuid: content}
        self._flat_catalog = {}
        for group in self._catalog:
            for content in group:
                self._flat_catalog[content['uuid']] = content

    def size(self):
        """ Return the total size of the catalog"""
        sizes = (s['media_size'] for s in self)
        return reduce(lambda x, y: x + y, sizes)

    def content(self, content_uuid):
        """ Return the content"""
        return self._flat_catalog[content_uuid]

    def content_types(self):
        """ Return the content types as dict: {content_type, content_nb}"""
        types = {}
        for content in self:
            ctype = content['category'][0]
            types.setdefault(ctype, 0)
            types[ctype] += 1
        return types

    def __iter__(self):
        return self._flat_catalog.itervalues()

    def len(self):
        """ Return the length of catalog"""
        return len(self._flat_catalog)
