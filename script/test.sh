#!/bin/sh

. ./script/env.sh

cp -fv "$PYTHON" "$ADAPTIVE_VIRTUAL_ENV/bin/"

cp -fv $ADAPTIVE_VIRTUAL_ENV/bin/activate $ADAPTIVE_VIRTUAL_ENV/bin/activate.old
cp -fv $ADAPTIVE_VIRTUAL_ENV/bin/pip $ADAPTIVE_VIRTUAL_ENV/bin/pip.old
cp -fv $ADAPTIVE_VIRTUAL_ENV/bin/gunicorn $ADAPTIVE_VIRTUAL_ENV/bin/gunicorn.old
cp -fv $ADAPTIVE_VIRTUAL_ENV/bin/ipython $ADAPTIVE_VIRTUAL_ENV/bin/ipython.old

sed -i 's/^VIRTUAL_ENV=.*$/VIRTUAL_ENV="$ADAPTIVE_VIRTUAL_ENV"/' "$ADAPTIVE_VIRTUAL_ENV/bin/activate"
sed -i "s|^\#\!.*$|\#\!$ADAPTIVE_VIRTUAL_ENV/bin/python|" "$ADAPTIVE_VIRTUAL_ENV/bin/pip"
sed -i "s|^\#\!.*$|\#\!$ADAPTIVE_VIRTUAL_ENV/bin/python|" "$ADAPTIVE_VIRTUAL_ENV/bin/gunicorn"
sed -i "s|^\#\!.*$|\#\!$ADAPTIVE_VIRTUAL_ENV/bin/python|" "$ADAPTIVE_VIRTUAL_ENV/bin/ipython"

. env/bin/activate

echo 'Python virtualenv:'
echo "VIRTUAL_ENV=$VIRTUAL_ENV"
echo "PATH=$PATH"

echo 'Now starting warm-up tests...'

echo 'Try to find Python...'
which python
if [ $? -ne 0 ]
then
  echo 'python not found in PATH !' 1>&2
  exit 1
fi

echo 'Check Python version is 2.7...'
cat <<-END | python
import sys
print 'Found: ' + sys.version
if sys.version_info.major != 2 or sys.version_info.minor != 7:
  sys.exit(1)
END
if [ $? -ne 0 ]
then
  echo 'Wrong version of Python found first in PATH ! (try $ python --version, we expect 2.7)' 1>&2
  exit 1
fi

echo 'Try to find SQLite...'
which sqlite3
if [ $? -ne 0 ]
then
  echo 'sqlite3 not found in PATH !' 1>&2
  exit 1
fi

echo 'Try to load SQLite from Python...'
cat <<-END | python
try:
  import sqlite3
  print 'Found: ' + sqlite3.__file__
except:
  import sys
  sys.exit(1)
END
if [ $? -ne 0 ]
then
  echo 'Python was not able to load sqlite3 module ! (be sure to build Python with SQLite support)' 1>&2
  exit 1
fi

echo 'Try to load Django from Python...'
cat <<-END | python
try:
  import django
  print 'Found: ' + django.__file__
except:
  import sys
  sys.exit(1)
END
if [ $? -ne 0 ]
then
  echo 'Python was not able to load django module !' 1>&2
  exit 1
fi

echo 'Check Django version is 1.9.4...'
cat <<-END | python
try:
  import django
  print 'Found: ' + django.get_version()
  if django.VERSION[0:3] != (1,9,4):
    if django.VERSION[0:3] != (1,9,2):
      import sys
      sys.exit(1)
except:
  import sys
  sys.exit(1)
END
if [ $? -ne 0 ]
then
  echo 'Wrong version of Django ! (we expect 1.7.4)' 1>&2
  exit 1
fi

echo 'All warm-up tests pass successfully.'



echo 'Now starting application tests...'

cd aces_onboard
python manage.py migrate
python manage.py collectstatic --no-input
cd ..

echo 'All application tests pass successfully.'



# echo 'Now starting synchronization tests...'
#
# #pushd $PWD >/dev/null
# cd $SYNC_FOLDER
# echo 'Try uplink synchronization...'
# date >sync_up.log
# python sync_up.py 2>&1 >>sync_up.log
# SYNC_RETURN_CODE=$?
# cat sync_up.log
# if [ $SYNC_RETURN_CODE -ne 0 ]
# then
#   echo 'Something is wrong with the uplink synchronization !' 1>&2
#   exit 1
# fi
#
# echo 'Try to upload sync log to S3...'
# s3cmd -c $ROOT/.s3cfg put sync_up.log s3://peplink.adaptive-channel.com/
# if [ $? -ne 0 ]
# then
#   echo 'S3cmd was not able to upload file to S3 !' 1>&2
#   exit 1
# fi
#
# echo 'Try downlink synchronization...'
# python sync_down.py 2>&1 >sync_down.log
# SYNC_RETURN_CODE=$?
# cat sync_down.log
# if [ $SYNC_RETURN_CODE -ne 0 ]
# then
#   echo 'Something is wrong with the downlink synchronization !' 1>&2
#   exit 1
# fi
#
# echo 'Try to move synchronized media files to the media folder...'
# mv -f *.jpg $MEDIA_FOLDER/ >>sync_down.log
# if [ $? -ne 0 ]
# then
#   echo 'Failed to move media files to the media folder !' 1>&2
#   exit 1
# fi
#
# s3cmd -c $ROOT/.s3cfg put sync_down.log s3://peplink.adaptive-channel.com/
# if [ $? -ne 0 ]
# then
#   echo 'S3cmd was not able to upload file to S3 !' 1>&2
#   exit 1
# fi
# cd ..
# #popd >/dev/null
# echo 'All synchronization tests pass successfully.'

deactivate

echo 'All tests pass successfully'
