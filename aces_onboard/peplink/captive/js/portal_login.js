// Start of Script (portal_login.js)

function setFocus() {
	try {
		if (document.login) {
			if (document.login.username.value.length > 0 ) {
					document.login.password.focus();
			} else {
					document.login.username.focus();
			}
		} else if (document.forgot_psw) {
			document.forgot_psw.reset_username.focus();
		} else if (document.change_psw) {
			document.change_psw.new_password.focus();
		} else if (document.reset_psw) {
		if (document.reset_psw.r.value.length == 0)
				document.reset_psw.r.focus();
			else
				document.reset_psw.new_password.focus();
		}
	} catch (err) {}
}
function setCookie(c_name,value,expiredays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate()+expiredays);
	document.cookie=c_name+ "=" +escape(value)+
	((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
}
function getCookie(c_name)
{
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++)
	{
		x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		x=x.replace(/^\s+|\s+$/g,"");
		if (x==c_name)
		{
			return unescape(y);
		}
	}
}
function onLogin(loginForm) {
	 username=loginForm.username.value;
	 setCookie('username',username,30);
	 return true;
}

// {{{ Common draw functions
var create_title_bar = function(text) {
	return create_div(null, "title_bg").append(
		create_div(null, "title_panel").append(
			create_p("title_text", "color_title_text", text)
		)
	);
};

var create_footer = function() {
	return create_div(null, "footer_bg").append(
		create_div(null, "footer_panel").append(
			create_p("", "copyright footer_text company-field", "")
		)
	);
};

var create_content_logo = function() {
	return create_div(null, "logo-panel").append(
			create_img("", "logo").attr("id", "logo-img")
		);
};

var get_company_field = function(company_name) {
	return "Powered by " + company_name + ".";
};

function register_refresh_position(obj)
{
	obj
	.on("common-refresh-position", function(e) {
		var	me = $(this);

		if ($(window).height() <= me.height())
			me.removeClass("abs_center").addClass("abs_notop_center");
		else
			me.removeClass("abs_notop_center").addClass("abs_center");
	})
	.on("register-resize-handle", function(e) {
		var	me = $(this);

		$(window).on("resize", function(e) {
			me.triggerHandler("common-refresh-position");
		});
	})
	.on("set-logo-handle", function(e, src) {
		var	me = $(this),
			loaded_src = false,
			delay = 3000,
			display_handle;

		display_handle = function()
		{
			me.triggerHandler("common-refresh-position");
			me.show();
		}

		if (!src)
		{
			display_handle()
		}

		me.find("#logo-img")
			.attr("src", src)
			.on("load", function() {
				me.triggerHandler("common-refresh-position");
				me.show();
				loaded_src = true;
			});

		setTimeout(function() {
			if (!loaded_src)
			{
				display_handle()
			}
		}, delay);
	});
}

var register_page_handler = function(root_obj, page_param) {
	if (!root_obj)
		return false;

	root_obj
	.data({
		"page_param": page_param
	})
	.on("change_style", function(e, key, prop, val) {
		var	me = $(this),
			page_param = me.data("page_param");

		if (!key || val == undefined)
			return false;

		page_param["style"][key][prop] = val;
	})
	.on("change_content", function(e, key, content) {
		var	me = $(this),
			page_param = me.data("page_param");

		if (!key || content == undefined)
			return false;

		page_param["content"][key] = content;
	})
	.on("set_footer_msg", function(e, content) {
		var	me = $(this),
			page_param = me.data("page_param");

		page_param["content"]["footer_text"] = content;
	})
	.on("login_refresh_position", function(e, t_obj, toogle_display) {
		var	me = $(this),
			win_height = $(window).height(),
			m_height;

		if (toogle_display)
			t_obj.show();

		m_height = t_obj.height();

		if (toogle_display)
			t_obj.hide();

		if (win_height <= m_height)
			me.removeClass("abs_center").addClass("abs_notop_center");
		else
			me.removeClass("abs_notop_center").addClass("abs_center");
	})
	.on("create_tnc_content", function(e) {
		var	me = $(this),
			page_param = me.data("page_param"),
			tnc_panel;

		tnc_panel =
		create_div(null, "content_container")
			.css({"background-color": page_param["style"]["main_content"]["background_color"]})
			.append(
				create_div("term_panel", "term_panel").append(
				create_textarea("tnc", "tnc", 10, null, "tnc")
					.css({
						"background-color": page_param["style"]["tnc_text"]["background_color"],
						"color": page_param["style"]["tnc_text"]["text_color"]
					})
					.html(page_param["content"]["tnc_text"])
					.attr("readonly", ""),
				create_textarea("tnc", "tnc_template", 10, null, "tnc").attr("readonly", "").hide(),
				create_div(null, "back_button_panel").append(
					create_input("back_login_button", "back_button", "button",
						(page_param["content"]["back_login_button"]?
							page_param["content"]["back_login_button"]: "Back to Login"))
						.attr("tabindex", 3)
						.on("click", function(e) {
							var	full_login = $("#full_login"),
								full_tnc = $("#full_tnc"),
								fadein_time = 500,
								fadeout_time = 100;

							e.preventDefault();

							full_tnc.fadeOut(fadeout_time, function() {
								full_tnc.attr("status", "");
								full_login.attr("status", "active");

								$("#main_container")
									.removeClass("tnc_width")
									.addClass("login_width");

								me.triggerHandler("login_refresh_position", [ full_login, true ]);
								full_login.fadeIn(fadein_time, function() {
							  	});
						  	});
						})
					)
				)
			);

		return tnc_panel;
	})
	.on("create_login_panel", function(e) {
		var	me = $(this), login_panel,
			page_param = me.data("page_param"),
			need_tnc = false,
			orig_url,
			tnc_agree_panel;

		setCookie('orig_url', page_param["orig_url"], 1);

		if (page_param["success_page_enable"])
			orig_url = page_param["cur_page_url"];
		else
			orig_url = page_param["orig_url"];

		if (page_param["style"]["tnc_text"]["display"])
		{
			var	agree_tnc_box, tnc_warn_panel,
				tnc_prompt = {"prefix": "",  "suffix": ""};

			need_tnc = true;

			agree_tnc_box =
				create_input("agree_tnc_box", null, "checkbox", "enable", "agree_tnc")
					.on("click", function(e) {
						var me = $(this);
						if (me.is(":checked"))
							tnc_warn_panel.hide();
					});

			tnc_warn_panel =
				create_div("tnc-warn-panel", "tnc_warn")
				.css({
					"background-color": page_param["style"]["tnc_accept_warn"]["background_color"],
					"color": page_param["style"]["tnc_accept_warn"]["text_color"]
				})
				.html(
					(page_param["content"]["tnc_accept_warn"]? page_param["content"]["tnc_accept_warn"]
					: "You must accept the terms and conditions before you can proceed")
				).hide();

			tnc_prompt["prefix"] =
				(page_param["content"]["tnc_prompt_prefix"]? page_param["content"]["tnc_prompt_prefix"]: "I have read and agree to the");
			tnc_prompt["suffix"] =
				(page_param["content"]["tnc_prompt_suffix"]? page_param["content"]["tnc_prompt_suffix"]: "");

			tnc_agree_panel = create_div(null, "tnc_agree_panel").append(
					agree_tnc_box,
					tnc_prompt["prefix"],
					create_hyperlink(" " + (page_param["content"]["tnc_title"]? page_param["content"]["tnc_title"]: "Terms and Conditions"), "#")
						.attr("id", "tnc_link")
						.on("click", function(e) {
							var	full_login = $("#full_login"),
								full_tnc = $("#full_tnc"),
								fadein_time = 500,
								fadeout_time = 100;

							e.preventDefault();

							full_login.fadeOut(fadeout_time, function() {
								$("#main_container")
									.removeClass("login_width")
									.addClass("tnc_width");

								me.triggerHandler("resize_tnc");

								full_tnc.attr("status", "active");
								full_login.attr("status", "");

								me.triggerHandler("login_refresh_position", [ full_tnc, true ]);
								full_tnc.fadeIn(fadein_time, function() {
							  	});
						  	});
						}),
					tnc_prompt["suffix"],
					tnc_warn_panel
				);
		}

		var submit_handle = function()
		{
			if (need_tnc && !agree_tnc_box.is(":checked"))
			{
				$("#tnc-warn-panel").show();
				agree_tnc_box.focus();
			}
			else
				form_obj.submit();
		}

		if (page_param["display_mode"] == "login")
		{
			var	form_obj = create_form("login", "POST", page_param["form_action"]);

			login_panel = create_div("login_panel", "login_panel").append(
				form_obj.append(
					create_div(null, "login_title", page_param["content"]["username_title"]),
					create_div(null, "input-panel").append(
						create_i(null, "fa fa-user icon-field"),
						create_span(null, "input-field").append(
							create_input("username", "enter-parm", "text", "", "username")
								.css({"color": page_param["style"]["input_placeholder"]["text_color"]})
								.attr("placeholder", page_param["content"]["username_placeholder"])
								.attr("maxlength", 32).attr("tabindex", 1)
						)
					),
					create_div(null, "login_title", page_param["content"]["password_title"]),
					create_div(null, "input-panel").append(
						create_i(null, "fa fa-key icon-field"),
						create_span(null, "input-field").append(
							create_input("password", "enter-parm", "password", "", "password")
								.css({"color": page_param["style"]["input_placeholder"]["text_color"]})
								.attr("placeholder", page_param["content"]["password_placeholder"])
								.attr("maxlength", 16).attr("tabindex", 2)
						)
					),
					create_div("fail-msg", "fail_message",
							(page_param["content"]["login_fail_msg"]? page_param["content"]["login_fail_msg"]: "Login Failed!")
						)
						.css({
							"display": (page_param["access_status"] == "fail"? "": "none"),
							"background-color": page_param["style"]["fail_message"]["background_color"],
							"color": page_param["style"]["fail_message"]["text_color"]
						}),
					(need_tnc ? tnc_agree_panel: ""),
					create_div(null, "login_button_panel").append(
						create_input(null, "submit_button", "button"
							, (page_param["content"]["login_button"]? page_param["content"]["login_button"]: "Login"))
							.attr("tabindex", 3)
							.on("click", function(e) {
								submit_handle();
							}),
						create_input(null, null, "hidden", "login_contenthub", "command"),
						create_input(null, null, "hidden", page_param["host_ip"], "host_ip"),
						create_input(null, null, "hidden", orig_url, "orig_url")
					)
				)
			);
		}
		else	// agree
		{
			var form_obj = create_form("login", "POST", page_param["form_action"]);

			var message_field = "<script language='javascript'>setTimeout(function() { $('form[name=\"login\"]').submit(); }, 1000);</script>";
			login_panel = create_div("login_panel", "login_panel").append(
				form_obj.append(
					(need_tnc ? tnc_agree_panel: ""),
					create_div(null, "open_login_button_panel").append(
						create_input(null, "submit_button", "button"
							, (page_param["content"]["connect_button"]? page_param["content"]["connect_button"]: "Connect"))
							.attr("tabindex", 3)
							.on("click", function(e) {
								submit_handle();
							}),
						create_input(null, null, "hidden", "login_contenthub", "command"),
						create_input(null, null, "hidden", "{SESSION_ID1}", "username"),
						create_input(null, null, "hidden", "{SESSION_ID2}", "password"),
						create_input(null, null, "hidden", page_param["host_ip"], "host_ip"),
						create_input(null, null, "hidden", orig_url, "orig_url"),
						create_div(null, "login_panel", message_field)
					)
				)
			)

		}

		return login_panel;
	})
	.on("create_popup_panel", function(e) {
		var	me = $(this), login_panel,
			page_param = me.data("page_param"),
			form_obj = create_form("", "POST", page_param["form_action"]);

			login_panel = create_div("login_panel", "login_panel").append(
				form_obj.append(
					create_div(null, "open_login_button_panel").append(
						create_input(null, "submit_button", "submit"
							, (page_param["content"]["logout_button"]? page_param["content"]["logout_button"]: "Logout")),
						create_input(null, null, "hidden", "logout", "command"),
						create_input(null, null, "hidden", page_param["orig_url"], "orig_url")
					)
				)
			);

		return login_panel;
	})
	.on("create_continue_panel", function(e) {
		var	me = $(this), login_panel,
			page_param = me.data("page_param"),
			action = page_param["orig_url"],
			form_obj = create_form("", "GET", action);

		var message_field = "<script language='javascript'>setTimeout(function() { $('.link').find('span').trigger('click'); }, 1000);</script>";
		var text = page_param["content"]["continue_button"]? page_param["content"]["continue_button"] : "Click to continue";

		login_panel = create_div("login_panel", "login_panel").append(
			form_obj.append(
				create_div(null, "open_login_button_panel").append(
					create_hyperlink(create_span(null, null, text), getCookie('orig_url')).addClass("link"),
					create_div(null, "login_panel", message_field)
				)
			)
		);
/*
		login_panel = create_div("login_panel", "login_panel").append(
			form_obj.append(
				create_div(null, "open_login_button_panel").append(
					create_hyperlink((page_param["content"]["continue_button"]? page_param["content"]["continue_button"]:"Click to continue"), getCookie('orig_url'))
				)
			)
		);
*/

		return login_panel;
	})
	.on("create_page_content", function(e) {
		var	me = $(this),
			page_param = me.data("page_param"),
			message_field = "",
			content_container, message_container, common_inner, main_content = "";

		if (page_param["command"] == "logout")
		{
			message_field
				= (page_param["content"]["logout_msg"]? page_param["content"]["logout_msg"]: "Logged Out Successfully!");
		}
		else if (page_param["command"] == "popup")
		{
			message_field
				= (page_param["content"]["popup_msg"]? page_param["content"]["popup_msg"]: "Thank you for using Portal service!");
			main_content = me.triggerHandler("create_popup_panel");
		}
		else if (page_param["command"] == "redirect")
		{
			// message_field = "Redirecting!<script language=\"javascript\">window.location.href=\"http://sita.aero/\"</script>";

			var	me = $(this), login_panel,
			page_param = me.data("page_param"),
			action = page_param["orig_url"],
			form_obj = create_form("", "GET", action);

			message_field = "<script language='javascript'>setTimeout(function() { $('.link').find('span').trigger('click'); }, 1000);</script>";
			var text = page_param["content"]["continue_button"]? page_param["content"]["continue_button"] : "Click to continue";

			login_panel = create_div("login_panel", "login_panel").append(
				form_obj.append(
					create_div(null, "open_login_button_panel").append(
						create_hyperlink(create_span(null, null, text), "http://sita.aero/").addClass("link"),
						create_div(null, "login_panel", message_field)
					)
				)
			);

			main_content = login_panel;
		}
		else
		{
			if (page_param["access_type"] == "login")
			{
				if (page_param["access_status"] == "fail" && page_param["auth_code"])
				{
					var auth_code = parseInt(page_param["auth_code"]);

					switch (auth_code)
					{
						case 401: // Invalid Username/Password
							page_param["display_mode"] = "login";
							message_field = page_param["content"]["welcome_msg"];
							main_content = me.triggerHandler("create_login_panel");
							break;
						case 200: // Login Success
						case 402: // Access Quota Reached
						case 500: // Internal server Error
						default:
							break;
					}
				}
				else
				{
					main_content = me.triggerHandler("create_continue_panel");
				}
			}
			else
			{
				message_field = page_param["content"]["welcome_msg"];
				main_content = me.triggerHandler("create_login_panel");
			}
		}

		common_inner =
			create_div(null, "common_inner")
				.css({"background-color": page_param["style"]["main_content"]["background_color"]})
				.append(create_content_logo());

		message_container = create_div(null, "message_container");

		if (message_field)
		{
			message_container.append(
				create_div("message-field", "message_panel", message_field)
					.css({
						"color": page_param["style"]["message"]["text_color"]
					})
			);
		}
		if (page_param["message"])
		{
			message_container.append(
				create_div(null, "message_panel", page_param["message"])
					.css({
						"color": page_param["style"]["message"]["text_color"]
					})
			);
		}
		if (page_param["auth_msg"])
		{
			message_container.append(
				create_div(null, "message_panel", page_param["auth_msg"])
					.css({
						"color": page_param["style"]["message"]["text_color"]
					})
			);
		}

		common_inner.append(
			message_container,
			main_content
		);

		content_container =
			create_div(null, "login-content-container")
				.append(common_inner);

		return content_container;
	})
	.on("create_page_content-bak", function(e) {
		var	me = $(this),
			page_param = me.data("page_param"),
			message_field = "",
			content_container, message_container, common_inner, main_content = "";

		if (page_param["command"] == "logout")
		{
			message_field
				= (page_param["content"]["logout_msg"]? page_param["content"]["logout_msg"]: "Logged Out Successfully!");
		}
		else if (page_param["command"] == "popup")
		{
			message_field
				= (page_param["content"]["popup_msg"]? page_param["content"]["popup_msg"]: "Thank you for using Portal service!");
			main_content = me.triggerHandler("create_popup_panel");
		}
		else
		{
			if (page_param["access_type"] == "login")
			{
				if (page_param["access_status"] == "fail")
				{
					page_param["display_mode"] = "login";
					message_field = page_param["content"]["welcome_msg"];
					main_content = me.triggerHandler("create_login_panel");
				}
				else
				{
					main_content = me.triggerHandler("create_continue_panel");
				}
			}
			else
			{
				message_field = page_param["content"]["welcome_msg"];
				main_content = me.triggerHandler("create_login_panel");
			}
		}

		common_inner =
			create_div(null, "common_inner")
				.css({"background-color": page_param["style"]["main_content"]["background_color"]})
				.append(create_content_logo());

		message_container = create_div(null, "message_container");

		if (message_field)
		{
			message_container.append(
				create_div("message-field", "message_panel", message_field)
					.css({
						"color": page_param["style"]["message"]["text_color"]
					})
			);
		}
		if (page_param["message"])
		{
			message_container.append(
				create_div(null, "message_panel", page_param["message"])
					.css({
						"color": page_param["style"]["message"]["text_color"]
					})
			);
		}
		if (page_param["auth_msg"])
		{
			message_container.append(
				create_div(null, "message_panel", page_param["auth_msg"])
					.css({
						"color": page_param["style"]["message"]["text_color"]
					})
			);
		}

		common_inner.append(
			message_container,
			main_content
		);

		content_container =
			create_div(null, "login-content-container")
				.append(common_inner);

		return content_container;
	})
	.on("redraw", function(e) {
		var	me = $(this),
			page_param = me.data("page_param"),
			show_header, show_footer;

		show_header = page_param["style"]["header"]["display"];
		show_footer = page_param["style"]["footer"]["display"];

		main_container =
			create_div("main_container", "login_width")
				.css({
					"color": page_param["style"]["main_content"]["text_color"]
				});
		main_container.append(
			create_div("full_login").attr("status", "active").append(
				create_title_bar(page_param["content"]["header_text"])
					.css({
						"display": (show_header? "": "none"),
						"background-color": page_param["style"]["header"]["background_color"],
						"color": page_param["style"]["header"]["text_color"]
					}),
				me.triggerHandler("create_page_content"),
				create_footer()
					.css({
						"display": (show_footer? "": "none"),
						"background-color": page_param["style"]["footer"]["background_color"],
						"color": page_param["style"]["footer"]["text_color"]
					})
			)
		);

		if (page_param["style"]["tnc_text"]["display"])
		{
			main_container.append(
				create_div("full_tnc").attr("status", "").hide().append(
					create_title_bar((page_param["content"]["tnc_title"]? page_param["content"]["tnc_title"]: "Terms and Conditions"))
						.css({
							"display": (show_header? "": "none"),
							"background-color": page_param["style"]["header"]["background_color"],
							"color": page_param["style"]["header"]["text_color"]
						}),
					me.triggerHandler("create_tnc_content"),
					create_footer()
						.css({
							"display": (show_footer? "": "none"),
							"background-color": page_param["style"]["footer"]["background_color"],
							"color": page_param["style"]["footer"]["text_color"]
						})
				)
			);
		}

		me.append(main_container);
	})
	.on("resize_tnc", function(e) {
		var me = $(this),
			win_height = $(window).height(),
			tnc_panel = $("#full_tnc"),
			is_full_tnc_display_toogle = (tnc_panel.css("display") == "none"),
			tnc_obj = $("#tnc"), tnc_template = $("#tnc_template"),
			max_tnc_height = 700, preset_tnc_height = 0, predict_panel_height = 0;

		if (is_full_tnc_display_toogle)
			tnc_panel.show();

		tnc_template.html(tnc_obj.html()).show();
		preset_tnc_height = tnc_template.prop("scrollHeight");
		tnc_template.hide();

		main_panel_height = me.height();

		if (is_full_tnc_display_toogle)
			tnc_panel.hide();

		predict_panel_height = main_panel_height + preset_tnc_height + 20;

		if (win_height < predict_panel_height)
		{
			if (preset_tnc_height > max_tnc_height)
				preset_tnc_height = max_tnc_height;
		}

		tnc_obj.css({"height": preset_tnc_height + "px"});
	})
	.on("submit_button_style_handle", function(e) {
		var	me =$(this),
			page_param = me.data("page_param");

		me.find(".submit_button")
			.css({
				"background-color": page_param["style"]["submit_button"]["background_color"],
				"color": page_param["style"]["submit_button"]["text_color"]
			});

		me.find(".back_button")
			.css({
				"background-color": page_param["style"]["back_button"]["background_color"],
				"color": page_param["style"]["back_button"]["text_color"]
			})
	})
	.on("init", function(e) {
		var	me =$(this),
			page_param = me.data("page_param"),
			background_img = page_param["content"]["background_img"],
			logo_src = (page_param["style"]["logo"]["display"]? page_param["content"]["logo"]: null);

		if (background_img)
			$("html").css('background-image', 'url("' + background_img + '")');

		me.triggerHandler("submit_button_style_handle");
		me.triggerHandler("set-logo-handle", [ logo_src ]);
		me.find(".company-field").html(page_param["content"]["footer_text"]);

		me.triggerHandler("register-resize-handle");
	});

	register_refresh_position(root_obj);
};

function change_content(key, content)
{
	var root_obj = $("#main_panel");
	root_obj.triggerHandler("change_content", [ key, content ]);
}

function hidden_element(key)
{
	var root_obj = $("#main_panel");
	root_obj.triggerHandler("change_style", [ key, "display", false ]);
}

function change_style(key, style_type, val)
{
	var root_obj = $("#main_panel");
	root_obj.triggerHandler("change_style", [ key, style_type, val ]);
}

function init_demo_page(page_param)
{
	var root_obj = $("#main_panel");
	register_page_handler(root_obj, page_param);
	root_obj.triggerHandler("redraw");
	root_obj.triggerHandler("init");
}
// }}}

// End of Script (portal_login.js)
