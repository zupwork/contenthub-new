import re

_regex_arp_table = re.compile(r'^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})\s+\w+\s+\w+\s+([0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2})\s+\*\s+\w+$')

def arp_table():
    blob = file.readlines(open('/proc/net/arp'))
    blob = blob[1:]
    tmp = map(lambda line: _regex_arp_table.match(line), blob)
    rez = {}
    for match in tmp:
        group = match.groups()
        rez[group[0]] = group[1]
    return rez
