#!/bin/sh

LOG_FILE=`pwd`/stop.log
date > ${LOG_FILE}

#
# Stop python
# - may use `kill -INT <PID>` to the child python process
#   for proper shutdown
#
echo "killing python `pidof python`..." >> ${LOG_FILE}
killall -KILL python

#
# Kill startup script (just in case)
#
# RETRY_COUNT=5
# while pidof adaptive_platform_tests.sh; do
# 	sleep 1
# 	if [ "${RETRY_COUNT}" -gt 0 ]; then
# 		RETRY_COUNT=$((${RETRY_COUNT} - 1))
# 	else
# 		echo "killing adaptive_platform_tests.sh `pidof adaptive_platform_tests.sh`..." >> ${LOG_FILE}
# 		killall adaptive_platform_tests.sh
# 	fi
# done
